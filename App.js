import React from "react";
import { Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";

// pages
// import Home from "./src/pages/Home";
import OrderScreenComponent from "./components/OrderScreenComponent";

function MessageScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>
        <AntDesign name="home" color={"red"} size={26} />
      </Text>
    </View>
  );
}
function DeliveryScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Delivery Screen</Text>
    </View>
  );
}
function ReceiptScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Receipt Screen</Text>
    </View>
  );
}
function UserScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>User Screen</Text>
    </View>
  );
}
function RecapScreen() {
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text>Recap Screen</Text>
    </View>
  );
}

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        activeColor="#ffffff"
        barStyle={{
          width: "100%",
          alignSelf: "center",
          backgroundColor: "#000000",
          height: '6%'
        }}
        tab
      >
        <Tab.Screen
          name="Home"
          component={OrderScreenComponent}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <AntDesign name="home" color={color} size={28} />
            ),
          }}
        />
        <Tab.Screen
          name="Message"
          component={MessageScreen}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="email-check-outline" color={color} size={28} />
            ),
          }}
        />
        <Tab.Screen
          name="Delivery"
          component={DeliveryScreen}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="truck-fast-outline" color={color} size={28} />
            ),
          }}
        />
        <Tab.Screen
          name="Receipt"
          component={ReceiptScreen}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <AntDesign name="printer" color={color} size={28} />
            ),
          }}
        />
        <Tab.Screen
          name="User"
          component={UserScreen}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <AntDesign name="user" color={color} size={28} />
            ),
          }}
        />
        <Tab.Screen
          name="Recap"
          component={RecapScreen}
          options={{
            tabBarLabel: false,
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="speedometer-slow" color={color} size={28} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

# Order Screen FE-1

This project is specifically for order screen. We built this using React Native as a framework, Yarn as a package manager, and Jest as a testing framework.

## How To Run The Project

Assumming that you're already installing expo-cli otherwise can follow their [documentation](https://docs.expo.io/workflow/expo-cli/#installation) and configure your xcode as their documentation for install [Xcode command line tools](https://docs.expo.io/workflow/ios-simulator/#step-2-install-xcode-command-line-tools)


1. Run the following command for installation package manager using Yarn:
 `yarn install`

2. Start the app by runnning  `yarn start` and then scan the barcode provided afterwards in your iPad, or if you're using iOS Simulator just type `i` for shortcut as the iOS.

## This app are tested using device:
1. Ipad Pro 11inch 2nd Gen - Portrait


## How to run the test

1. Run the following command for test suites `yarn test`

2. For code coverage use this command `yarn test --coverage`
